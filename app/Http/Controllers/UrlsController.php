<?php

namespace App\Http\Controllers;

use App\Jobs\UrlsJob;
use App\Models\Url;
use Illuminate\Http\Request;

class UrlsController extends Controller
{
    public function index()
    {
        $urls = Url::all();
        foreach ($urls as $url) {
            UrlsJob::dispatchIf($url->status == "NEW", $url);
        }

        return redirect()->back()->with(['message' => 'Urlovi']);
    }
}
