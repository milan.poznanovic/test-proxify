<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    /**
     * @var string
     */
    protected $table = 'urls';

    /**
     * @var string[]
     */
    protected $fillable = ['url', 'status', 'http_code'];

}
