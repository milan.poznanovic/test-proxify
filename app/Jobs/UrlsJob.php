<?php

namespace App\Jobs;

use App\Models\Url;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UrlsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;

    /**
     * Create a new job instance.
     *
     * UrlsJob constructor.
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $this->url->update(['status' => 'PROCESSING']);

        $client = new Client();
        try {
            $response = $client->request('GET', $this->url->url, ['verify' => false, 'http_errors' => false]);
            $statusCode = $response->getStatusCode();
            $status = ($statusCode >= 200 and $statusCode < 300) ? 'DONE' : ' ERROR';

            $this->url->update([
                'http_code' => $statusCode,
                'status' => $status,
            ]);

        } catch (\Exception $e) {
            $this->url->update([
                'status' => 'ERROR',
            ]);
        }
    }
}
