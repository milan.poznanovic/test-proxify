<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ModelsUrl;
use Faker\Generator as Faker;

$factory->define(App\Models\Url::class, function (Faker $faker) {
    return [
        'url' =>$faker->url,
        'status' => 'NEW'
    ];
});
